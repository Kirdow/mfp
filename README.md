# Minecraft Function Preprocessor #

This program is a preprocessor for function files used for minecraft.
Simply open the function file using this program and it should output
one with all the things compiled into normal minecraft functions.

### What is this repository for? ###

This repository is just so I can share the code with the world as I'm feeling this doesn't require a closed source.

### How can I help/contribute? ###

At this point I'm most likely not gonna use any external help, but if you have anything that could be changed, feel free to point it out.

### This has been made before ###

Yes, it has, and I'm actually inspired by a guy who made RPL which was Redstone Programming Language, a python filter for MCEdit which turned the RPL code into command blocks.
His creation inspired me to make this preprocessor program since it featured source-time variables which functions does not feature at this day.